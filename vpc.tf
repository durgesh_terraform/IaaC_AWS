resource "aws_vpc" "iaac_vpc" {
    provider = aws
    cidr_block = "10.235.16.0/24"
    instance_tenancy = "default"
    enable_dns_support = "true"
    enable_dns_hostnames = "true"
    enable_classiclink = "false"

    tags = {
      "Name" = var.vpc_name
    }
}