
resource "aws_instance" "t2micro_core-ap-south-1a" {
  ami = var.AWS_AMIS[var.region]
  instance_type = "t2.micro"
  key_name = var.AWS_PUBLIC_KEY

  subnet_id = data.aws_subnet.aws_ap-south-1a-core.id
  vpc_security_group_ids = [aws_security_group.aws_allowssh.id]
}