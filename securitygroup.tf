resource "aws_security_group" "aws_allowssh" {
  vpc_id = aws_vpc.iaac_vpc.id
  name = "aws_allowssh"

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "aws_allowssh"
  }
}