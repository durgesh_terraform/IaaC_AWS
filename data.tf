#Fetch availability zones
data "aws_availability_zones" "azs" {
    provider = aws
}

#Fetch core subnet in ap-south-1a
data "aws_subnet" "aws_ap-south-1a-core" {
  vpc_id = aws_vpc.iaac_vpc.id
  provider = aws

  /* filter {
    name = "tag:Name"
    values = ["core-ap-south-1a"]
  } */

  tags = {
      Name = "core-ap-south-1a"
  }
}