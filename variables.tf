variable "AWS_SECRET" {
  
}

variable "AWS_KEY" {
  
}

variable "region" {
  default = "ap-south-1"
}

variable "vpc_name" {
    default = "iaac_vpc"
}

variable "csvfilename" {
    default = "./subnets.csv"
}

#10.235.16.0 - Development
#10.235.17.0 - QA
#10.235.18.0 - UAT
#10.235.19.0 - NAT
#10.235.20.0 - EKS
variable "vpc_subnets" {
    default = ["10.235.17.0/24", "10.235.18.0/24", "10.235.19.0/24", "10.235.20.0/24"]
}

locals {
  raw_subnets = csvdecode(file(var.csvfilename))
}

# Map of AMIs
variable "AWS_AMIS" {
  type = map(string)
  default = {
    "ap-south-1" = "ami-0c1a7f89451184c8b"
  }
}

# Public Key Name
variable "AWS_PUBLIC_KEY" {
  type = string
  default = "terraformkey" #This needs to be present in AWS KMS for ssh connection later
}
