#Associate subnets with vpc
#See definition of vps_subnets in variables.tf
resource "aws_vpc_ipv4_cidr_block_association" "aws_subnet_cidr" {
  for_each = { for idx, subnet in var.vpc_subnets : idx => subnet}
  vpc_id = aws_vpc.iaac_vpc.id
  cidr_block = each.value

  provider = aws
}

#Create subnets from csv
resource "aws_subnet" "aws_subnets" {
  for_each = { for idx, subnet in local.raw_subnets : idx => subnet }
  vpc_id = aws_vpc.iaac_vpc.id
  availability_zone = lower(each.value.az)
  cidr_block = "${each.value.addresses}/${each.value.netmask}"
  provider = aws
  map_public_ip_on_launch = "true" #Remove this for private subnets 

  tags = {
    NetworkType = each.value.networktype
    Name = "${each.value.networktype}-${each.value.az}"
  }
}

#Group subnets into AZs
locals {
  use1a = {
      for idx, subnet in aws_subnet.aws_subnets : idx => subnet
      if lower(subnet.availability_zone) == "ap-south-1a"
  }

  use1b = {
      for idx, subnet in aws_subnet.aws_subnets : idx => subnet
      if lower(subnet.availability_zone) == "ap-south-1b"
  }

  use1c = {
      for idx, subnet in aws_subnet.aws_subnets : idx => subnet
      if lower(subnet.availability_zone) == "ap-south-1c"
  }
}

#Internet Gateway
resource "aws_internet_gateway" "aws_gateway" {
  vpc_id = aws_vpc.iaac_vpc.id

  tags = {
    Name = "aws_gateway"
  }
  
}

#Route Table
resource "aws_route_table" "aws_routetable" {
  vpc_id = aws_vpc.iaac_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.aws_gateway.id
  }

  tags = {
    Name = "aws_routetable"
  }
}

#Associate Subnets to route tables
resource "aws_route_table_association" "aws_routetable_assoc1a" {
  for_each = local.use1a
  subnet_id = each.value.id
  route_table_id = aws_route_table.aws_routetable.id

  provider = aws
}

resource "aws_route_table_association" "aws_routetable_assoc1b" {
  for_each = local.use1b
  subnet_id = each.value.id
  route_table_id = aws_route_table.aws_routetable.id

  provider = aws
}

resource "aws_route_table_association" "aws_routetable_assoc1c" {
  for_each = local.use1c
  subnet_id = each.value.id
  route_table_id = aws_route_table.aws_routetable.id

  provider = aws
}
