terraform {
  required_providers {
      aws = {
          source = "hashicorp/aws"
          version = "~> 3.0"
      }
  }
}

provider "aws" {
  region = var.region
  access_key = var.AWS_KEY
  secret_key = var.AWS_SECRET
}
